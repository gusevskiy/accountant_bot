import telebot
from telebot import types
from telebot.types import KeyboardButton

#import config

#API_TOKEN = config.TOKEN

#bot = telebot.TeleBot(API_TOKEN)
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    print(message.text)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(KeyboardButton('Отправить свой контакт ☎️', request_contact=True)) #Имена кнопок
    markup.add(KeyboardButton('Отправить свою локацию 🗺️', request_location=True)) #Имена кнопок
    msg = bot.reply_to(message, 'Test text', reply_markup=markup)
    bot.register_next_step_handler(msg, process_step)

def func1(message):
    bot.send_message(message.chat.id, 'ну и')

def func2(message):
    bot.send_message(message.chat.id, 'bbb')

def process_step(message):
    chat_id = message.chat.id
    if message.text=='1':
        func1(chat_id)
    else:
        func2(chat_id)

bot.polling(none_stop=True)